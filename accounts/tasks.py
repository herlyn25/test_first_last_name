from celery import shared_task # Don't create dependency with your main project


@shared_task
def add(x, y):
    return x + y


@shared_task
def multiply(x, y):
    return x * y
